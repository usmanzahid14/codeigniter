<!DOCTYPE html>
<html>
<head>
<title>Edit User</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>css/nayyarTech.css" type="text/css" />


</head>

 
<body>

<div class="container">
  <div class="row">
    <div class="span12">
      <div class="head">
         <div class="row-fluid">

                <div>
                <img src="<?php echo base_url();?>download.png" style="height: 135px;">
                </div>
                     
            </div>  
        </div>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/list">Explore Packages</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/upload">Upload Package</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userForm">Register User</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userslist">Users List</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<form  action="<?php echo base_url().'index.php/users/updateUser' ?>" method="post" >
      <div class="container">
        <h2>Edit User Details</h2>
        <p>Update User Details to Register</p>
        <hr>
        <input type="hidden" name="id"   title="id" value="<?php echo $data->id; ?>">  


        <label for="username"><b>User Name</b></label>
         <?php echo form_input(['name'=>'username', 'name'=>'username','placeholder'=>'User   Name','class'=>'form-control',
                 'value'=>set_value('name',$data->username)]);  ?> 

        <label for="email"><b>Email</b></label>
         <input type="text" name="email"   title="id" value="<?php echo $data->email; ?>" readonly>  
 

         <label for="password"><b>Password</b></label>
          <?php echo form_input(['name'=>'password', 'name'=>'password','placeholder'=>'Email  ','class'=>'form-control',
                 'value'=>set_value('name',$data->password)]);  ?> 
        

                 <div class="form-group">
                    <label for="sel1">Select Package:</label>
                     <select class="form-control" name="name">
                       <?php 
                       
                          foreach($all->result() as $row)
                             {
                              ?>

                              <option  value="<?php echo $row->name; ?>"
                                ><?php echo $row->name; ?>
                              </option>
                            
                              <?php
                             }
                        
                      ?>   
                      </select>
                  </div>
        <hr>
     
        <button type="submit" class="btn btn-primary">Register</button>
      </div>
      
      <div class="container signin">
        <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>
</form>






 



</body>
</html>