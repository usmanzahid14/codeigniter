<!DOCTYPE html>
<html>
<head>
<title>Add User</title>

 <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>css/nayyarTech.css" type="text/css" />

</head>

<body>
<div class="container">
  <div class="row">
    <div class="span12">
      <div class="head">
         <div class="row-fluid">

                <div>
                <img src="<?php echo base_url();?>download.png" style="height: 135px;">
                </div>
                     
            </div>  
        </div>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/list">Explore Packages</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/upload">Upload Package</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userForm">Register User</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userslist">Users List</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<form  action="<?php echo base_url().'index.php/users/addUser' ?>" method="post" >
 <div class="container">
        <h2>User Details</h2>
        <p>Enter User Details to Register</p>
        <hr>

         <div class="form-group">
                    <label for="name"><strong>Select Package:</strong></label>
                     <select class="form-control" name="name">
                       <?php 
                       
                          foreach($all->result() as $row)
                             {
                              ?>

                              <option  value="<?php echo $row->name; ?>"
                                ><?php echo $row->name; ?>
                              </option>
                            
                              <?php
                             }
                        
                      ?>   
                      </select>
                  </div>
 
        <label for="username"><b>User Name</b></label>
         <input type="text" required="" placeholder="User Name"  value="<?php echo set_value('username');?>" name="username" class="txt">

        <label for="email"><b>Email</b></label>
         <input type="text" required="" value="<?php echo set_value('email');?>" placeholder="User Email" name="email" class="txt">

         <label for="password"><b>Password</b></label>
        <input type="password" required="" value="<?php echo set_value('password');?>" placeholder="Enter Password" name="password" class="txt">
        

                
        <hr>
     
        <button type="submit" class="btn btn-primary">Register</button>
</div>
      
       
</form>


 





</body>
</html>