<!DOCTYPE html>
<html>
<head>
<title>Edit Package</title>

 <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>css/nayyarTech.css" type="text/css" />
</head>
 
<body>

<div class="container">
  <div class="row">
    <div class="span12">
      <div class="head">
         <div class="row-fluid">

                <div>
                <img src="<?php echo base_url();?>download.png" style="height: 135px;">
                </div>
                     
            </div>  
        </div>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/list">Explore Packages</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/packageTask/upload">Upload Package</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userForm">Register User</a>
                        </li>

                        <li>
                            <a href="http://localhost/nayyarTechnologies/index.php/users/userslist">Users List</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?php echo validation_errors(); ?>
<form  action="<?php echo base_url().'index.php/packageTask/updatePackage' ?>" method="post" >
      <div class="container">
        <h2>Packages</h2>
        <p>Please fill in this form to create an account.</p>
        <hr>
        <input type="hidden" name="id"   title="id" value="<?php echo $data->id; ?>">  

        <label for="email"><b>Package Name</b></label>
         <?php echo form_input(['name'=>'name', 'name'=>'name','placeholder'=>'Package   Name','class'=>'form-control',
                 'value'=>set_value('name',$data->name)]);  ?>

        <label for="description"><b>Description</b></label>
         <?php echo form_input(['name'=>'description', 'name'=>'description','placeholder'=>'Package Description','class'=>'form-control',
                 'value'=>set_value('name',$data->description)]);  ?> 

         
        <hr>
     
        <button type="submit" class="btn btn-primary">Register</button>
      </div>
      
       
</form>







</body>
</html>